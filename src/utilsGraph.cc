#include "utilsGraph.hh"

#include <cmath>
#include <memory>

#include "graph.hh"
#include "node.hh"

void
UtilsGraph::generateNode(Graph& g,
			 std::pair<double,double> min,
			 std::pair<double,double> max)
{
    double distX = max.first - min.first;
    double distY = max.second - min.second;
    int freq = 100000;
    int nbPoints = (distX * distY) / freq;

    std::cout << "[LOG] Generating " << nbPoints << " nodes." << std::endl;

    for (int i = 0; i < nbPoints; ++i)
    {
	double x = static_cast<double>(rand() % (int)(distX)) - distX + max.first;
	double y = static_cast<double>(rand() % (int)(distY)) - distY + max.second;
	g.addNode(std::make_shared<Node>(x, y));
    }
}

void
UtilsGraph::generateEdge(Graph& g,
			 const std::vector<ArLineSegment>& lines)
{
    const std::vector<std::shared_ptr<Node>>& nodes = g.getNodes();
    std::cout << "[LOG] Generating edges." << std::endl;
    for (auto firstNode = nodes.cbegin() + 1; firstNode != nodes.cend(); ++firstNode)
    {
	for (auto secondNode = firstNode + 1; secondNode != nodes.cend(); ++secondNode)
	{
	    ArLineSegment line((*firstNode)->getPosition().first,
			       (*firstNode)->getPosition().second,
			       (*secondNode)->getPosition().first,
			       (*secondNode)->getPosition().second);

	    bool intersection = false;
	    for (auto lineIt = lines.cbegin();
		 lineIt != lines.cend() && !intersection;
		 ++lineIt)
	    {
		ArPose useless;
		if (lineIt->intersects(&line, &useless))
		    intersection = true;
	    }

	    if (!intersection)
	    {
		g.addEdge(*firstNode, *secondNode);
		g.addEdge(*secondNode, *firstNode);
		(*firstNode)->addNeighbor(*secondNode);
		(*secondNode)->addNeighbor(*firstNode);
	    }
	}
    }
}

double
UtilsGraph::distance(std::shared_ptr<Node> A,
		     std::shared_ptr<Node> B)
{
    double distx = B->getPosition().first - A->getPosition().first;
    double disty = B->getPosition().second - A->getPosition().second;

    return sqrt(distx * distx + disty * disty);
}
