#pragma once

#include <memory>
#include <vector>

class Node
{
public:
    Node(double x, double y);
    Node(std::pair<double,double> position);
    const std::pair<double,double> getPosition() const;
    void addNeighbor(std::shared_ptr<Node> node);
    const std::vector<std::shared_ptr<Node>>& getNeighbors() const;

    bool operator==(const Node& other) const;

    std::pair<double,double> position_;
private:
    std::vector<std::shared_ptr<Node>> neighbors_;
};
