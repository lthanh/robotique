#include <iostream>
#include <vector>
#include <utility>
#include "Aria.h"

#define pi 3.141592654
#define kx 5
#define ky 500
#define kth 100
#define Cycle 20
#define Te Cycle*1e-3
#define a 0.1                      //vitesse de trj d�sir�e

class PrintingTask
{
    public:
        // Constructor. Adds our 'user task' to the given robot object.
        PrintingTask(ArRobot *robot);

        // Destructor. Does nothing.
        ~PrintingTask(void) {}

        // This method will be called by the callback functor
        void doTask(void);
    protected:
        ArRobot *myRobot;
        FILE *file;
        int i,j;         // indices utiles
        double angle[4] = {0, pi/2, pi, -pi/2};
        std::vector<std::pair<double, double>> destination;
        // The functor to add to the robot for our 'user task'.
        ArFunctorC<PrintingTask> myTaskCB;
};


// the constructor (note how it uses chaining to initialize myTaskCB)
PrintingTask::PrintingTask(ArRobot *robot) :
    myTaskCB(this, &PrintingTask::doTask)
{
    myRobot = robot;
    // just add it to the robot
    myRobot->addSensorInterpTask("PrintingTask", 50, &myTaskCB);
    file = ArUtil::fopen("data.dat", "w+");
    i=0;
    j=1;
    double currentX = myRobot->getX();
    double currentY = myRobot->getY();
    double edgeD = 10;

    std::pair<double, double> d1 = std::make_pair(currentX, currentY);
    std::pair<double, double> d2 = std::make_pair(currentX + edgeD, currentY);
    std::pair<double, double> d3 = std::make_pair(currentX + edgeD, currentY + edgeD);
    std::pair<double, double> d4 = std::make_pair(currentX, currentY + edgeD);
    destination.push_back(d1);
    destination.push_back(d2);
    destination.push_back(d3);
    destination.push_back(d4);
}

void PrintingTask::doTask(void)
{
    double x,y,th,t;                      //variables de sortie
    double ex,ey,eth;                   //les erreurs
    double v,w;                         //variables de commande
    double vr,wr;                       //consignes de vitesse
    double l,xt,yt,tht;                 //variables dues aux consignes
    int c1,c2,c3,c4,c5,c6,c7,c8;
    //calcul des trajectoires d�sir�es
    //std::cout << "Diff= " << angle[j % 5] - myRobot->getTh()*(pi/180) << std::endl; 
    //std::cout << "Current Angle= " << angle[j%5] << std::endl; 
    //std::cout << "Current Direction= " << myRobot->getTh() << std::endl; 
    if (i < j*500)
    {
        //std::cout << "Advancing" << std::endl;
        wr=0;
        vr=0.01;

        tht=angle[(j-1) % 5];
        xt = destination[j].first;
        yt = destination[j].second;
        i++;
    }
    else if (i == j*500 && (angle[j%5] - myRobot->getTh()*(pi/180) < -0.05 || angle[j%5] - myRobot->getTh()*(pi/180) > 0.05))
    {
        //std::cout << "Rotating" << std::endl;
        wr=0.05;
        vr=0;

        tht=angle[j%5];
        xt=myRobot->getX()*1e-3;
        yt=myRobot->getY()*1e-3;

    }
    else
        j++;

    //i++;
    // r�cup�rer some info about the robot
    x=myRobot->getX()*1e-3;
    y=myRobot->getY()*1e-3; 
    th=myRobot->getTh()*(pi/180);

    // calcul des erreurs
    ex= (xt-x)*cos(th)+(yt-y)*sin(th);
    ey= -(xt-x)*sin(th)+(yt-y)*cos(th);
    eth=tht-th;

    //calcul des consignes
    v=vr*cos(eth)+kx*ex;
    w=wr+vr*sin(ky*ey+kth*(eth));

    // envoi des consignes
    myRobot->lock();
    myRobot->setRotVel(w*(180/pi));
    myRobot->setVel(v*1000);
    myRobot->unlock();

    fprintf(file, "%f, %f, %f, %f, %f, %f, %f %f %f\n", x,y,th,xt,yt,tht,t);

    // Need sensor readings? Try myRobot->getRangeDevices() to get all 
    // range devices, then for each device in the list, call lockDevice(), 
    // getCurrentBuffer() to get a list of recent sensor reading positions, then
    // unlockDevice().
}

int main(int argc, char** argv)
{
    // the connection
    ArSimpleConnector con(&argc, argv);
    if(!con.parseArgs())
    {
        con.logOptions();
        return 1;
    }

    // robot
    ArRobot robot;

    // sonar array range device
    ArSonarDevice sonar;

    // This object encapsulates the task we want to do every cycle. 
    // Upon creation, it puts a callback functor in the ArRobot object
    // as a 'user task'.
    PrintingTask pt(&robot);

    // the actions we will use to wander
    ArActionStallRecover recover;
    ArActionAvoidFront avoidFront;
    ArActionConstantVelocity constantVelocity("Constant Velocity", 400);

    // initialize aria
    Aria::init();

    // add the sonar object to the robot
    robot.addRangeDevice(&sonar);

    // open the connection to the robot; if this fails exit
    if(!con.connectRobot(&robot))
    {
        printf("Could not connect to the robot.\n");
        return 2;
    }
    printf("Connected to the robot. (Press Ctrl-C to exit)\n");


    // turn on the motors, turn off amigobot sounds
    robot.comInt(ArCommands::ENABLE, 1);
    robot.comInt(ArCommands::SOUNDTOG, 0);

    // add the wander actions
    /*robot.addAction(&recover, 100);
      robot.addAction(&avoidFront, 50);
      robot.addAction(&constantVelocity, 25);
      */
    robot.setCycleTime(Cycle);
    // Start the robot process cycle running. Each cycle, it calls the robot's
    // tasks. When the PrintingTask was created above, it added a new
    // task to the robot. 'true' means that if the robot connection
    // is lost, then ArRobot's processing cycle ends and this call returns.
    robot.run(true);

    printf("Disconnected. Goodbye.\n");

    return 0;
}
