#pragma once

#include <iostream>
#include <memory>

#include "node.hh"

class Edge
{
public:
    Edge(std::shared_ptr<Node> from, std::shared_ptr<Node> to);

public:
    const std::shared_ptr<Node> getFromNode() const;
    const std::shared_ptr<Node> getToNode() const;
    bool operator==(const Edge& other) const;
    bool operator!=(const Edge& other) const;

private:
    std::shared_ptr<Node> from_;
    std::shared_ptr<Node> to_;
};
