#include <iostream>

#include "Aria.h"
#include "astar.hh"
#include "graph.hh"
#include "utilsGraph.hh"
#include <cstdlib>

int main(int argc, char **argv)
{
    ArMap map;
    map.readFile(argv[1]);
    std::vector<ArLineSegment>* lines = map.getLines();

    ArPose lineMin = map.getLineMinPose();
    ArPose lineMax = map.getLineMaxPose();

    Graph g;
    UtilsGraph::generateNode(g,
    			     std::pair<double,double>(lineMin.getX(),
    						      lineMin.getY()),
    			     std::pair<double,double>(lineMax.getX(),
    						      lineMax.getY()));

    std::shared_ptr<Node> start;
    ArMapObject *obj = map.findMapObject("RobotHome");
    if (obj)
    {
	start = std::make_shared<Node>(obj->getPose().getX(),
				       obj->getPose().getY());
    }

    std::shared_ptr<Node> dest  = std::make_shared<Node>((double) atoi(argv[2]),
							 (double) atoi(argv[3]));

    g.addNode(start);
    g.addNode(dest);

    UtilsGraph::generateEdge(g, *lines);
    Astar astar;
    std::vector<std::shared_ptr<Node>> path = astar.computeAstar(start, dest);

}
