#include <iostream>
#include <vector>
#include <utility>
#include <memory>
#include "node.hh"
#include "Aria.h"
#include "astar.hh"
#include "graph.hh"
#include "utilsGraph.hh"

#define pi 3.141592654
#define kx 5
#define ky 500
#define kth 100
#define Cycle 20
#define Te Cycle*1e-3
#define a 0.1                      //vitesse de trj désirée

class PrintingTask
{
    public:
        // Constructor. Adds our 'user task' to the given robot object.
        PrintingTask(ArRobot *robot, std::vector<std::shared_ptr<Node>>& v);

        // Destructor. Does nothing.
        ~PrintingTask(void) {}

        // This method will be called by the callback functor
        void doTask(void);
        bool checkPos(std::tuple<double, double, double>& action);
        void getActions(double x, double y, double tht);
        bool checkDst(std::tuple<double,double,double>& action);
    protected:
        ArRobot *myRobot;
        FILE *file;
        int i,j;         // indices utiles
        double angle[4] = {0, pi/2, pi, -pi/2};
        std::vector<std::pair<double, double>> destination;
        // The functor to add to the robot for our 'user task'.
        ArFunctorC<PrintingTask> myTaskCB;
        std::vector<std::tuple<double,double,double>> actions;
};


// the constructor (note how it uses chaining to initialize myTaskCB)
PrintingTask::PrintingTask(ArRobot *robot, std::vector<std::shared_ptr<Node>>& v) :
    myTaskCB(this, &PrintingTask::doTask)
{
    myRobot = robot;
    // just add it to the robot
    myRobot->addSensorInterpTask("PrintingTask", 50, &myTaskCB);
    file = ArUtil::fopen("data.dat", "w+");
    i=0;
    j=0;
    for (const auto& c :v)
    {
        getActions(c->getPosition().first,c->getPosition().second,0);
        std::cout << "x= " << c->position_.first << " y= " << c->position_.second << std::endl;
    }
}

void PrintingTask::getActions(double x, double y, double tht)
{
    double orgX = myRobot->getX();
    double orgY = myRobot->getY();
    double currentAngle = myRobot->getTh();
    double op = x - orgX;
    double adj = y - orgY;
    double tan = op / adj;
    double angle = atan(tan) * 180/pi;
    std::cout << "Angle = " << angle << std::endl;
    double rotAngle = angle + currentAngle > 180 ? ((angle + currentAngle) - (static_cast<int>(angle + currentAngle) % 180)) * -1 : angle + currentAngle;
    double distance = sqrt(op*op + adj*adj);
    actions.push_back(std::make_tuple(orgX,orgY, rotAngle * pi/180));
    actions.push_back(std::make_tuple(x,y,rotAngle *pi/180)); 
    actions.push_back(std::make_tuple(x,y,tht));
}

bool PrintingTask::checkDst(std::tuple<double,double,double>& action)
{
    /*std::cout << "Check dire = " << std::get<2>(action) << std::endl;
    std::cout << "Check current d = " << myRobot->getTh() *pi/180  << std::endl;
    std::cout << "Diff = " << fabs(std::get<2>(action) - myRobot->getTh() * pi/180) << std::endl*/;
    return (fabs(std::get<0>(action) - myRobot->getX()*1e-3) < 0.1
            && fabs(std::get<1>(action) - myRobot->getY()*1e-3) < 0.1
            && fabs(std::get<2>(action) - myRobot->getTh() * pi/180) < 0.05);
}

bool PrintingTask::checkPos(std::tuple<double, double, double>& action)
{
    /*std::cout << "diff X = " << fabs(std::get<0>(action) - myRobot->getX()*1e-3) << std::endl;
    std::cout << "diff Y = " << fabs(std::get<1>(action) - myRobot->getY()*1e-3) << std::endl;*/
    return (fabs(std::get<0>(action) - myRobot->getX()*1e-3) < 0.1
            && fabs(std::get<1>(action) - myRobot->getY()*1e-3) < 0.1);
}

void PrintingTask::doTask(void)
{
    double x,y,th,t;                      //variables de sortie
    double ex,ey,eth;                   //les erreurs
    double v,w;                         //variables de commande
    double vr,wr;                       //consignes de vitesse
    double l,xt,yt,tht;                 //variables dues aux consignes
    int c1,c2,c3,c4,c5,c6,c7,c8;
    //calcul des trajectoires désirées
    //std::cout << "Diff= " << angle[j % 5] - myRobot->getTh()*(pi/180) << std::endl; 
    //std::cout << "Current Angle= " << angle[j%5] << std::endl; 
    //std::cout << "Current state "  << j << std::endl;
    //std::cout << "Current Direction= " << myRobot->getTh() << std::endl; 
    //std::cout << "X= " << myRobot->getX()*1e-3 << std::endl;
    //std::cout << "Y= " << myRobot->getY()*1e-3 << std::endl;
    if (j%3==0)
    {
        wr = 0.05;
        vr = 0;
    }
    else if (j%3 == 1)
    {
        wr = 0;
        vr = 0.5;
    }
    else
    {
        vr = 0;
        wr = 0.05;
    }
    if (j > actions.size())
    {
        return;
    } 
    if (checkDst(actions[j]))
    {
        ++j;
    }
    else if (j%3 == 1 && checkPos(actions[j]))
    {
        ++j;
    }
    else
    {
        xt = std::get<0>(actions[j]);      
        yt = std::get<1>(actions[j]);      
        tht = std::get<2>(actions[j]);
        /*
        std::cout << "xt= " << xt << std::endl;
        std::cout << "yt= " << yt << std::endl;

        std::cout << "target Direction " << tht <<std::endl;*/ 
    }


    x=myRobot->getX()*1e-3;
    y=myRobot->getY()*1e-3; 
    th=myRobot->getTh()*(pi/180);

    // calcul des erreurs
    ex= (xt-x)*cos(th)+(yt-y)*sin(th);
    ey= -(xt-x)*sin(th)+(yt-y)*cos(th);
    eth=tht-th;

    //calcul des consignes
    v=vr*cos(eth)+kx*ex;
    w=wr+vr*(ky*ey+kth*sin(eth));

    // envoi des consignes
    myRobot->lock();
    myRobot->setRotVel(w*(180/pi));
    myRobot->setVel(v*1000);
    myRobot->unlock();

    fprintf(file, "%f, %f, %f, %f, %f, %f, %f %f %f\n", x,y,th,xt,yt,tht,t);

}

int main(int argc, char** argv)
{
    // the connection
    Aria::init();
    ArMap map;
    map.readFile(argv[1]);
    std::vector<ArLineSegment>* lines = map.getLines();

    ArPose lineMin = map.getLineMinPose();
    ArPose lineMax = map.getLineMaxPose();

    Graph g;
    UtilsGraph::generateNode(g,
    			     std::pair<double,double>(lineMin.getX(),
    						      lineMin.getY()),
    			     std::pair<double,double>(lineMax.getX(),
    						      lineMax.getY()));

    std::shared_ptr<Node> start  = std::make_shared<Node>((double) atoi(argv[2]),
							  (double) atoi(argv[3]));
    //double startAngle = (double) atoi(argv[4]);
    std::shared_ptr<Node> dest  = std::make_shared<Node>((double) atoi(argv[4]),
							 (double) atoi(argv[5]));

    g.addNode(start);
    g.addNode(dest);

    UtilsGraph::generateEdge(g, *lines);
    Astar astar;
    std::vector<std::shared_ptr<Node>> path = astar.computeAstar(start, dest);
    path.pop_back();
    std::vector<std::shared_ptr<Node>> newpath(path.rbegin(),path.rend());
    for (auto& n : newpath)
    {
        std::cout << n->position_.first;
        std::cout << n->position_.second;

    }
    for (auto& n : newpath)
    {
        /*if (n->position_.first == dest->position_.first
                && n->position_.second == dest->position_.second)
        { */
            n->position_.first -= start->position_.first;
            n->position_.second -= start->position_.second;
            n->position_.first *= 1e-3;
            n->position_.second *= 1e-3;
        //}
    }
    // robot
    ArRobot robot;
    ArArgumentParser parser(&argc, argv);

    ArRobotConnector robotConnector(&parser, &robot);

    // sonar array range device
    ArSonarDevice sonar;

    // This object encapsulates the task we want to do every cycle. 
    // Upon creation, it puts a callback functor in the ArRobot object
    // as a 'user task'.
    PrintingTask pt(&robot,newpath);
    // the actions we will use to wander
    ArActionStallRecover recover;
    ArActionAvoidFront avoidFront;
    ArActionConstantVelocity constantVelocity("Constant Velocity", 400);

    // initialize aria

    // add the sonar object to the robot
    robot.addRangeDevice(&sonar);

    // open the connection to the robot; if this fails exit
    printf("Connected to the robot. (Press Ctrl-C to exit)\n");


    // turn on the motors, turn off amigobot sounds
    robot.comInt(ArCommands::ENABLE, 1);
    robot.comInt(ArCommands::SOUNDTOG, 0);

    // add the wander actions
    /*robot.addAction(&recover, 100);
      robot.addAction(&avoidFront, 50);
      robot.addAction(&constantVelocity, 25);
      */
    robot.setCycleTime(Cycle);
    // Start the robot process cycle running. Each cycle, it calls the robot's
    // tasks. When the PrintingTask was created above, it added a new
    // task to the robot. 'true' means that if the robot connection
    // is lost, then ArRobot's processing cycle ends and this call returns.
    robot.run(true);

    printf("Disconnected. Goodbye.\n");

    return 0;
}
