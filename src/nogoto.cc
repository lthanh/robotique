#include <iostream>
#include <vector>
#include <utility>
#include <memory>
#include "node.hh"
#include "Aria.h"
#include "astar.hh"
#include "graph.hh"
#include "utilsGraph.hh"

#define pi 3.141592654
#define kx 5
#define ky 500
#define kth 100
#define Cycle 20
#define Te Cycle*1e-3
#define a 0.1                      //vitesse de trj désirée

int main(int argc, char** argv)
{
    // the connection
    Aria::init();
    ArMap map;
    map.readFile(argv[1]);
    std::vector<ArLineSegment>* lines = map.getLines();

    ArPose lineMin = map.getLineMinPose();
    ArPose lineMax = map.getLineMaxPose();

    Graph g;
    UtilsGraph::generateNode(g,
            std::pair<double,double>(lineMin.getX(),
                lineMin.getY()),
            std::pair<double,double>(lineMax.getX(),
                lineMax.getY()));

    std::shared_ptr<Node> start  = std::make_shared<Node>((double) atoi(argv[2]),
            (double) atoi(argv[3]));
    //double startAngle = (double) atoi(argv[4]);
    std::shared_ptr<Node> dest  = std::make_shared<Node>((double) atoi(argv[4]),
            (double) atoi(argv[5]));

    g.addNode(start);
    g.addNode(dest);

    UtilsGraph::generateEdge(g, *lines);
    Astar astar;
    std::vector<std::shared_ptr<Node>> path = astar.computeAstar(start, dest);
    path.pop_back();
    //std::vector<std::shared_ptr<Node>> newpath(path.rbegin(),path.rend());
    
    ArRobot robot;

    // sonar array range device
    ArSonarDevice sonar;
    ArArgumentParser parser(&argc, argv);                           

    ArRobotConnector robotConnector(&parser, &robot); 
    if(!robotConnector.connectRobot())
    {
        ArLog::log(ArLog::Terse, "gotoActionExample: Could not connect to the robot.");
        if(parser.checkHelpAndWarnUnparsed())
        {
            Aria::logOptions();
            Aria::exit(1);
        }
    }
    robot.moveTo(ArPose(start->position_.first, start->position_.second));   
    //
    // This object encapsulates the task we want to do every cycle. 
    // Upon creation, it puts a callback functor in the ArRobot object
    ArActionLimiterForwards limiterAction("speed limiter near", 300, 600, 250);
    ArActionLimiterForwards limiterFarAction("speed limiter far", 300, 1100, 400);
    ArActionLimiterTableSensor tableLimiterAction;
    ArActionGoto gotoPoseAction("goto");
    robot.addAction(&gotoPoseAction, 100);

    ArActionStop stopAction("stop");
    robot.addAction(&stopAction, 40);
    //
    robot.runAsync(true);
    robot.lock();
    robot.enableMotors();
    robot.unlock();
    robot.comInt(ArCommands::SOUNDTOG, 0);

    std::cout << "Connected !!!!" << std::endl;
    bool first = true;

    while (Aria::getRunning())
    {
        robot.lock();
        if (first || gotoPoseAction.haveAchievedGoal())
        {
            if (path.size() == 0)
                break;
            first = false;
            gotoPoseAction.setGoal(ArPose(path.back()->position_.first, path.back()->position_.second));
            path.pop_back();
        }

        ArLog::log(ArLog::Normal, "Going to next goal at %.0f %.0f", 
                gotoPoseAction.getGoal().getX(), gotoPoseAction.getGoal().getY());
        robot.unlock();
        ArUtil::sleep(100);
    }
    std::cout << "Finished " << std::endl;
    return 0;
}
