#include "graph.hh"

void
Graph::addNode(std::shared_ptr<Node> node)
{
    nodes_.push_back(node);
}

void
Graph::addEdge(std::shared_ptr<Node> from, std::shared_ptr<Node> to)
{
    // Note : add both direction
    //        Fix it if we want directed graph
    std::shared_ptr<Edge> go = std::make_shared<Edge>(from, to);
    std::shared_ptr<Edge> back = std::make_shared<Edge>(to, from);
    edges_.push_back(go);
    edges_.push_back(back);
}

const std::vector<std::shared_ptr<Edge>>&
Graph::getEdges() const
{
    return edges_;
}

const std::vector<std::shared_ptr<Node>>&
Graph::getNodes() const
{
    return nodes_;
}
