#pragma once

#include <memory>
#include <vector>

#include "edge.hh"
#include "node.hh"

class Graph
{
public:
    void addNode(std::shared_ptr<Node> node);
    void addEdge(std::shared_ptr<Node> from, std::shared_ptr<Node> to);
    const std::vector<std::shared_ptr<Edge>>& getEdges() const;
    const std::vector<std::shared_ptr<Node>>& getNodes() const;

private:
    std::vector<std::shared_ptr<Edge>> edges_;
    std::vector<std::shared_ptr<Node>> nodes_;
};
