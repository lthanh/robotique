#include "edge.hh"

Edge::Edge(std::shared_ptr<Node> from, std::shared_ptr<Node> to)
    : from_(from)
    , to_(to)
{
}

const std::shared_ptr<Node>
Edge::getFromNode() const
{
    return from_;
}

const std::shared_ptr<Node>
Edge::getToNode() const
{
    return to_;
}

bool
Edge::operator==(const Edge& other) const
{
    return from_ == other.from_ && to_ == other.to_;
}

bool
Edge::operator!=(const Edge& other) const
{
    return !operator==(other);
}
