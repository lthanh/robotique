#include "node.hh"

Node::Node(double x, double y)
    : position_(std::pair<double,double>(x, y))
{
}

Node::Node(std::pair<double,double> position)
    : position_(position)
{
}

const std::pair<double,double>
Node::getPosition() const
{
    return position_;
}

void
Node::addNeighbor(std::shared_ptr<Node> node)
{
    neighbors_.push_back(node);
}

const std::vector<std::shared_ptr<Node>>&
Node::getNeighbors() const
{
    return neighbors_;
}

bool
Node::operator==(const Node& other) const
{
    return this->position_.first == other.position_.first &&
           this->position_.second == other.position_.second;
}
