#pragma once

#include "Aria.h"

#include <memory>
#include <utility>
#include <vector>

class Graph;
class Node;

class UtilsGraph
{
public:
    static void generateNode(Graph& g,
			     std::pair<double,double> min,
			     std::pair<double,double> max);
    static void generateEdge(Graph& g,
			     const std::vector<ArLineSegment>& lines);
    static double distance(std::shared_ptr<Node> A,
			   std::shared_ptr<Node> B);

};
