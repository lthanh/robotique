#include "astar.hh"

#include "utilsGraph.hh"

namespace
{
    class HeapComparator
    {
    public:
	HeapComparator(std::map<std::shared_ptr<Node>,
		       std::shared_ptr<NodeInfo>> nodeinfos)
	    : nodeInfos_(nodeinfos)
	{ }

	bool operator()(std::shared_ptr<Node> a, std::shared_ptr<Node> b)
	{
	    return nodeInfos_[a]->fcost < nodeInfos_[b]->fcost;
	}

    private:
	std::map<std::shared_ptr<Node>, std::shared_ptr<NodeInfo>> nodeInfos_;
    };
}

std::vector<std::shared_ptr<Node>>
Astar::computeAstar(std::shared_ptr<Node> start,
		    std::shared_ptr<Node> end)
{
    std::vector<std::shared_ptr<Node>> path;

    nodeInfos_.clear();
    heap_.clear();

    // Init
    std::shared_ptr<NodeInfo> info =
	std::make_shared<NodeInfo>(UtilsGraph::distance(start, end),
				   0,
				   true,
				   false,
				   start);

    nodeInfos_.insert(std::pair<std::shared_ptr<Node>,std::shared_ptr<NodeInfo>>
		      (start, info));
    heap_.push_back(start);
    std::push_heap(heap_.begin(), heap_.end(), HeapComparator(nodeInfos_));

    while (!heap_.empty())
    {
	std::shared_ptr<Node> cur = heap_.back();
	std::pop_heap(heap_.begin(), heap_.end(), HeapComparator(nodeInfos_));
	heap_.pop_back();
	nodeInfos_[cur]->closed = true;
	if (cur == end)
	    break;

	addNeighbors(cur, end);
    }

    if (nodeInfos_[end] && nodeInfos_[end]->closed)
    {
	std::shared_ptr<Node> to = end;
	path.push_back(end);
	while (true)
	{
            std::shared_ptr<Node> from = nodeInfos_[to]->parent;
	    path.push_back(from);
	    if (from == start)
		break;
	    to = from;
	}
    }

    return path;
}

void
Astar::addNeighbors(std::shared_ptr<Node> cur,
		    std::shared_ptr<Node> end)
{
    for (std::shared_ptr<Node> n : cur->getNeighbors())
    {
        computeInfos(n, cur, end);
    }
}

void
Astar::computeInfos(std::shared_ptr<Node> cur,
		    std::shared_ptr<Node> prev,
		    std::shared_ptr<Node> end)
{
    double wayCost = UtilsGraph::distance(cur, end);
    double g = nodeInfos_[prev]->gcost + wayCost;
    int h = UtilsGraph::distance(cur, end);

    std::map<std::shared_ptr<Node>, std::shared_ptr<NodeInfo>>::iterator it =
	nodeInfos_.find(cur);

    if (it == nodeInfos_.end()) // Not found
    {
	std::shared_ptr<NodeInfo> info =
	    std::make_shared<NodeInfo>(g + h,
				       g,
				       true,
				       false,
				       prev);

	nodeInfos_.insert(std::pair<std::shared_ptr<Node>,std::shared_ptr<NodeInfo>>
			  (cur, info));
        heap_.push_back(cur);
	std::push_heap(heap_.begin(), heap_.end(), HeapComparator(nodeInfos_));
    }
    else
    {
        if ((g + h) < (*it).second->fcost)
        {
            (*it).second->fcost = g + h;
            (*it).second->gcost = g;
            (*it).second->parent = prev;
        }
    }
}
