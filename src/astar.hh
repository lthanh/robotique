#pragma once

#include <cmath>
#include <map>
#include <vector>

#include "edge.hh"
#include "graph.hh"
#include "node.hh"

class Traveler;

struct NodeInfo
{
    NodeInfo()
        : fcost(0)
        , gcost(0)
        , visited(false)
        , closed(false)
        , parent(nullptr)
    { }

    NodeInfo(double f, double g, bool visit, bool close, std::shared_ptr<Node> p)
        : fcost(f)
        , gcost(g)
        , visited(visit)
        , closed(close)
        , parent(p)
    { }

    std::shared_ptr<Node> cur;
    std::shared_ptr<Node> parent;
    double fcost;
    double gcost;
    bool visited;
    bool closed;
};


class Astar
{
public:

public:
    void setKnowledge(std::map<Edge, int>* knowledge);
    std::vector<std::shared_ptr<Node>> computeAstar(std::shared_ptr<Node> start,
						    std::shared_ptr<Node> end);


private:
    void addNeighbors(std::shared_ptr<Node> cur,
		      std::shared_ptr<Node> end);
    void computeInfos(std::shared_ptr<Node> cur,
		      std::shared_ptr<Node> prev,
		      std::shared_ptr<Node> end);

private:
    std::map<std::shared_ptr<Node>, std::shared_ptr<NodeInfo>> nodeInfos_;
    std::vector<std::shared_ptr<Node>> heap_;
};
