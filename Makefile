# PROJET ROBOTIQUE

PROJNAME = robot
CXXFLAGS = -std=c++14 -I/usr/local/Aria/include/
LIB = -lAria -L/usr/local/Aria/lib -ldl -pthread
CXX = g++
SRC = src/graph.cc src/edge.cc src/node.cc src/utilsGraph.cc \
      src/astar.cc
SRC1 = src/goto.cc $(SRC)
SRC2 = src/nogoto.cc $(SRC)
OBJ1 = $(SRC1:.cc=.o)
OBJ2 = $(SRC2:.cc=.o)

notgoto: $(OBJ2)
	$(CXX) $(OBJ2) -o $(PROJNAME) $(LIB)

clean:
	$(RM) $(PROJNAME) $(OBJ)

goto: $(OBJ1)
	$(CXX) $(OBJ1) -o $(PROJNAME) $(LIB)

test:
	$(CXX) $(CXXFLAGS) src/test.cc -o test $(LIB)

check: test
	./test /usr/local/MobileSim/AMROffice.map

debug: CXXFLAGS+=-g3
debug: all
